public class Test {
  public void f() {
  }

  public int m(int x) {
    return x * x;
  }

  // Not part of the outline because it is private:
  void g() {
  }
}

class X {
}
