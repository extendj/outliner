Outliner Demo
=============

This is a demo extension to the extensible Java compiler ExtendJ.

This extension is a tool that prints an outline for input Java classes. This
extension demonstrates JastAdd synthesized attributes, inter-type declarations,
and collection attributes.

Cloning this Project
--------------------

To clone this project you will need [Git][3] installed.

Use this command to clone the project using Git:

    git clone --recursive git@bitbucket.org:extendj/outliner.git

The `--recursive` flag makes Git also clone the ExtendJ submodule while cloning
the `outliner` repository.

If you forgot the `--recursive` flag you can manually clone the ExtendJ
submodule using these commands:

    cd outliner
    git submodule init
    git submodule update

This should download the ExtendJ Git repository into a local directory named
`extendj`.

Build and Run
-------------

If you have [Gradle][1] installed you can issue the following commands to
build and test the minimal extension:

    gradle jar
    java -jar outliner.jar testfiles/Test.java

If you do not have Gradle installed you can use the `gradlew.bat` (on Windows)
or `gradlew` (Mac/Linux) script instead. For example to build on Windows run the
following in a command prompt:

    gradlew jar

The `gradlew` scripts are wrapper scripts that will download Gradle locally and
run it.

How it Works
------------

The `src/jastadd/ExtensionBase.jrag` file contains all code for printing Java
class outlines.

The `CompilationUnit.process()` method is called for each Java source file
parsed with no semantic errors.  This inter-type declaration iterates over each
type declaration in each compilation unit and prints the outline.

The `TypeDecl.printOutline()` method is the method used to print the outline
for a type declaration (i.e.  a class, enum, interface etc.). It uses a collection attribute
called `TypeDecl.methods()`. The `methods()` attribute is declared like this:


    coll Collection<MethodDecl> TypeDecl.methods() [new LinkedList<>()] with add root TypeDecl;

    MethodDecl contributes this when isPublic() to TypeDecl.methods() for hostType();

The first line declares the attribute, giving the type for the collection, the
initialization expression (`new LinkedList<>()`), the method used to update the
collection (`with add`), and the root node for the collection (`root
TypeDecl`).

The second line gives a contribution to the attribute for `MethodDecl` AST
nodes. Each method declaration contributes itself to the `TypeDecl.methods()`
attribute. The last part of the contributes statement gives an expression for
which node the contribution contributes to, in this case the node referred to
by the inherited `hostType()` attribute.

Extension Architecture
----------------------

The architecture for ExtendJ extensions is documented in [the Extension Base project][4].


Rebuilding
==========

Although the Gradle plugin can handle some automatic rebuilding when a source file changes,
it does not handle all cases well, so in some cases you will need to force Gradle to
rebuild your project. This can be done passing the `--rerun-tasks` option to Gradle:

    $ gradle --rerun-tasks


Changes to the `jastadd_modules` file always require a rebuild to ensure that everything
is generated from the current sources.


Additional Resources
--------------------

More examples on how to build ExtendJ-like projects with the [JastAdd Gradle
plugin][2] can be found here:

* [Extension Base: a minimal ExtendJ extension][4]
* [JastAdd Example: GradleBuild](http://jastadd.org/web/examples.php?example=GradleBuild)

[1]:https://gradle.org/
[2]:https://bitbucket.org/joqvist/jastaddgradle/overview
[3]:https://git-scm.com/
[4]:https://bitbucket.org/extendj/extension-base
